#!/usr/bin/env python3

from heaserver.fileawss3 import service
from heaserver.service.testcase import swaggerui
from heaserver.service.testcase.testenv import MicroserviceContainerConfig
from heaserver.service.wstl import builder_factory
from heaserver.service.testcase.dockermongo import DockerMongoManager
from heaserver.service.testcase.awsdockermongo import S3WithDockerMongoManager
from aiohttp.web import get, post, put, delete, view, options
from integrationtests.heaserver.fileawss3integrationtest.fileawss3testcase import db_values
from heaobject.registry import Resource
from heaobject.volume import DEFAULT_FILE_SYSTEM
from heaobject.volume import AWSFileSystem
import logging

logging.basicConfig(level=logging.DEBUG)

HEASERVER_REGISTRY_IMAGE = 'registry.gitlab.com/huntsman-cancer-institute/risr/hea/heaserver-registry:1.0.0'
HEASERVER_VOLUMES_IMAGE = 'registry.gitlab.com/huntsman-cancer-institute/risr/hea/heaserver-volumes:1.0.0'
HEASERVER_BUCKETS_IMAGE = 'registry.gitlab.com/huntsman-cancer-institute/risr/hea/heaserver-buckets:1.0.0'
HEASERVER_ACTIVITY_IMAGE = 'registry.gitlab.com/huntsman-cancer-institute/risr/hea/heaserver-activity:1.0.0'

if __name__ == '__main__':
    volume_microservice = MicroserviceContainerConfig(image=HEASERVER_VOLUMES_IMAGE, port=8080, check_path='/volumes',
                                                db_manager_cls=DockerMongoManager,
                                                resources=[Resource(resource_type_name='heaobject.volume.Volume',
                                                                    base_path='volumes',
                                                                    file_system_name=DEFAULT_FILE_SYSTEM),
                                                           Resource(resource_type_name='heaobject.volume.FileSystem',
                                                                    base_path='filesystems',
                                                                    file_system_name=DEFAULT_FILE_SYSTEM)])
    bucket_microservice = MicroserviceContainerConfig(image=HEASERVER_BUCKETS_IMAGE, port=8080, check_path='/ping',
                                                db_manager_cls=DockerMongoManager,
                                                resources=[Resource(resource_type_name='heaobject.folder.Folder',
                                                                    base_path='volumes',
                                                                    file_system_type=str(AWSFileSystem),
                                                                    file_system_name=DEFAULT_FILE_SYSTEM)],
                                                env_vars={'HEA_MESSAGE_BROKER_ENABLED': 'false'})
    activity_microservice = MicroserviceContainerConfig(image=HEASERVER_ACTIVITY_IMAGE, port=8080,
                                                check_path='/desktopobjectactions',
                                                db_manager_cls=DockerMongoManager,
                                                resources=[Resource(resource_type_name='heaobject.activity.DesktopObjectAction',
                                                                    base_path='desktopobjectactions')])
    swaggerui.run(project_slug='heaserver-files-aws-s3', desktop_objects={k: v for k, v in db_values.items() if not isinstance(k, str) and k.db_manager_cls is DockerMongoManager},
                  wstl_builder_factory=builder_factory(service.__package__),
                  routes=[(get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}', service.get_file),
                          (put, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}', service.put_file),
                          (options, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}', service.get_file_options),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/', service.get_files),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/byname/{name}', service.get_file_by_name),
                          (view, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}/opener', service.get_file_opener),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}/duplicator', service.get_file_duplicator),
                          (post, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}/duplicator',
                           service.post_file_duplicator),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}/mover', service.get_file_mover),
                          (post, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}/mover', service.post_file_mover),
                          (options, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files', service.get_files_options),
                          (delete, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}', service.delete_file),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}/content', service.get_file_content),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}/presignedurl', service.get_presigned_url_form),
                          (post, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}/presignedurl', service.post_presigned_url_form),
                          (put, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}/content', service.put_file_content),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}/unarchive', service.get_file_unarchive_form),
                          (post, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}/unarchive', service.unarchive_file),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}/archive', service.get_file_archive),
                          (post, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{id}/archive', service.post_file_archive  ),
                          (options, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{file_id}/versions/{id}', service.get_version_options),
                          (delete, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{file_id}/versions/{id}', service.delete_version),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{file_id}/versions/{id}', service.get_version),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3files/{file_id}/versions/', service.get_versions)
                   ], registry_docker_image=HEASERVER_REGISTRY_IMAGE,
                  db_manager_cls=S3WithDockerMongoManager,
                  other_docker_images=[volume_microservice, activity_microservice, bucket_microservice])
